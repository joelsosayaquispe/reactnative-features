import { StatusBar } from 'expo-status-bar';
import { Button, ScrollView, StyleSheet, Text, View } from 'react-native';
import CameraScreen from './src/features/camerascreen/camera-screen';

export default function App() {
	return (
		<ScrollView>
			<Button title="Camera screen" />
			<CameraScreen />
		</ScrollView>
		// <View style={styles.container}>
		//   <Text>Open up App.tsx to start working on yousr app!</Text>
		//   <StatusBar style="auto" />
		// </View>
	);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
		alignItems: 'center',

		justifyContent: 'space-around',
	},
});
