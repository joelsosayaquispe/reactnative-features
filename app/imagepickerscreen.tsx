import { Button, Text, View } from "react-native";
import { launchCamera } from "react-native-image-picker";

const ImagePickerScreen = ()=>{
  
  const pickPhoto = async()=>{
    const result = await launchCamera({cameraType: 'front',mediaType: 'photo'});
  }
  return (
    <View className="h-screen flex items-center justify-center">
      <Text>Image Picker </Text>
      <Button title="pick photo" onPress={pickPhoto}/>
    </View>
  );
}

export default ImagePickerScreen;