import { Text, View } from 'react-native';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
const Tab = createBottomTabNavigator();
const TabNavigatorScreen = () => {
	return (
		<Tab.Navigator
			screenOptions={({ route }) => ({
				tabBarIcon: ({ focused, color, size }) => {
					let iconName;

					if (route.name === 'Home') {
						iconName = focused
							? 'ios-information-circle'
							: 'ios-information-circle-outline';
					} else if (route.name === 'Settings') {
						iconName = focused ? 'list-outline' : 'ios-list-outline';
					}

					// You can return any component that you like here!
					return <Ionicons name='list-outline' size={size} color={color} />;
				},
				tabBarActiveTintColor: 'tomato',
				tabBarInactiveTintColor: 'gray',
			})}
		>
			<Tab.Screen name="Home" component={HomeScreen} />
			<Tab.Screen name="Settings" component={SettingsScreen} />
		</Tab.Navigator>
	);
};

export default TabNavigatorScreen;
const HomeScreen = () => {
	return (
		<View>
			<Text>home screen</Text>
		</View>
	);
};

const SettingsScreen = () => {
	return (
		<View>
			<Text>settings screen</Text>
		</View>
	);
};
