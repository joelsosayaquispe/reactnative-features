import { Text, View } from "react-native";
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
const Drawer = createDrawerNavigator();
const DrawerScreen = () => {
  return (
    
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name="Home" component={HomeScreen} />
        <Drawer.Screen name="Notifications" component={NotificationsScreen} />
      </Drawer.Navigator>
    
  );
};

export default DrawerScreen;

const HomeScreen = ()=>{
  return (
    <View>
      <Text >home screen</Text>
    </View>
  );
}

const NotificationsScreen = ()=>{
  return (
    <View>
      <Text >notifications screen</Text>
    </View>
  );
}
