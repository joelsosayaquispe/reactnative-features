import i18next from 'i18next';
import { useTranslation } from 'react-i18next';
import { Button, Text, View } from 'react-native';

const MultilanguageScreen = () => {
  const {t} = useTranslation();
	return <View>
    <Text>{t('title')}</Text>
    <Button title='spanish' onPress={()=>i18next.changeLanguage('es')} />
    <Button title='english' onPress={()=>i18next.changeLanguage('en')} />
  </View>;
};

export default MultilanguageScreen;
