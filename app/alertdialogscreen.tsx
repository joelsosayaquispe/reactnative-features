import { Text, Alert, View,TouchableOpacity } from 'react-native';

const AlertDialogScreen = () => {
	const showAlertDialog = () => {
		Alert.alert(
			'Alert Title',
			'My Alert Msg',
			[
				{
					text: 'Cancel',
					onPress: () => Alert.alert('Cancel Pressed'),
					style: 'cancel',
				},
        
			],
      
			{
				cancelable: true,
				onDismiss: () =>
					Alert.alert(
						'This alert was dismissed by tapping outside of the alert dialog.'
					),
			}
		);
	};

	return (
		<View>
			<TouchableOpacity  onPress={showAlertDialog} >
        <Text>show alert dialog</Text>
      </TouchableOpacity>
		</View>
	);
};

export default AlertDialogScreen;
