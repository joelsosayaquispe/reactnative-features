import { useState } from "react";
import { StyleSheet, Switch, Text, TextInput, TouchableOpacity, View } from "react-native";
import { RadioButton } from 'react-native-paper';
const FormScreen = () => {

  const [name, setName] = useState("");
  const [currentRadio,setCurrentRadio] = useState('mayonesa')
  const handleForm = ()=>{
    console.log(name)
    
  }
  return (
    <View className="h-screen flex items-center justify-center">
      <Text>Formulario </Text>
      <TextInput style={styles.input} onChangeText={setName} placeholder="Enter name" value={name} className="border border-black" />
      
      <Text>Switch button</Text>
      <Switch  value={true} onChange={()=>{

      }} />
      <Text>Radio button</Text>
      <RadioButton value="mayonesa" status={currentRadio === 'mayonesa' ? "checked": "unchecked"}
      onPress={(value)=>{
        
        setCurrentRadio("mayonesa");
      }} />
      <RadioButton value="mostaza" status={currentRadio === 'mostaza' ? "checked": "unchecked"}
      onPress={(value)=>{
        
        setCurrentRadio("mostaza");
      }} />
      <RadioButton value="ketchup" status={currentRadio === 'ketchup' ? "checked": "unchecked"} 
      onPress={(value)=>{
        
        setCurrentRadio("ketchup");
      }}/>
      <TouchableOpacity className="rounded-md p-4 bg-gray-200" onPress={handleForm}>
        <Text>Guardar</Text>
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  input:{
    padding: 10,
    backgroundColor: '#f0f0f0',
    width: '100%',
  }
})
export default FormScreen;