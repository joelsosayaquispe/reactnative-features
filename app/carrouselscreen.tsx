import { useRef, useState } from "react";
import { Animated, Dimensions, Text, View } from "react-native"
import { PageIndicator } from "react-native-page-indicator";
import Carousel from "react-native-reanimated-carousel";

const CarrouselScreen = ()=> {
    const width = Dimensions.get('window').width;
    const scrollx = useRef(new Animated.Value(0)).current;
    const [currentItem,setCurrentItem] = useState(0);
    
    return (

        <View style={{ flex: 1 }}>
        <Carousel
            loop
            width={width}
            height={width / 2}
            autoPlay={true}
            snapEnabled={true}
           
            data={[...new Array(6).keys()]}
            scrollAnimationDuration={1000}
            onSnapToItem={(index) => {
                console.log('current index:', index)
                setCurrentItem(index);
                    // scrollx.setValue(index);
            }}
            renderItem={({ index }) => (
                <View
                    style={{
                        flex: 1,
                        borderWidth: 1,
                        justifyContent: 'center',
                    }}
                >
                    <Text style={{ textAlign: 'center', fontSize: 30 }}>
                        {index}
                    </Text>
                </View>
            )}
        />
        <PageIndicator count={[...new Array(6).keys()].length} current={currentItem} />
    </View>
    )
};

export default CarrouselScreen;