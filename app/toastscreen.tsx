import { Button, Text, View } from "react-native";
import Toast from "react-native-toast-message";

const ToastScreen = ()=>{
 
  const showToast = () => {
    
    Toast.show({
      type: 'success',
      text1: 'Hello',
      position: 'bottom',
      visibilityTime: 1000,
      text2: 'This is some something 👋'
    });
  }
  return (
    <View className="h-screen">
      
      <Text >toast screen</Text>
      <Button title="toast" onPress={showToast} ></Button>
      <Toast />
    </View>
  );
} 

export default ToastScreen;