import { StatusBar, StatusBarStyle, Text, View } from "react-native";
import { Appbar, PaperProvider } from "react-native-paper";

const AppBarScreen = ()=>{

    return (
        <PaperProvider>
            <Appbar.Header>
       <Appbar.Content title="Title" subtitle={'Subtitle'} />
        <Appbar.Action icon="magnify" onPress={() => {}} />
        
    </Appbar.Header>
        <View>
            <StatusBar 
             animated={true}
             backgroundColor="#61dafb"
             hidden={false}
            ></StatusBar>
            <Text></Text>
        </View>
        </PaperProvider>
    );
};

export default AppBarScreen;