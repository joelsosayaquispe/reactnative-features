import { useState } from 'react';
import { Button, Modal, Text, View } from 'react-native';

const ModalExampleScreen = () => {
	const [isModal, setIsModal ] = useState(false);
	return (
		<View className="h-screen">
			<Button title="show modal" onPress={() => setIsModal(true)} />

			<Modal  visible={isModal} animationType="slide"
        transparent={true} >
				<View className=" bg-purple-500  h-3/5 absolute bottom-0 w-full">
					<Text>MODAL</Text>
          <Button title='close' onPress={()=>setIsModal(false)}/>
				</View>
			</Modal>
		</View>
	);
};

export default ModalExampleScreen;
