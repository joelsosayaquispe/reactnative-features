import { Link } from "expo-router";
import { useState } from "react";
import { StatusBarStyle, Text, TouchableOpacity, View } from "react-native";

const indexScreen = () => {
  return (
    <View className="bg-blue-600 text-center flex justify-around items-center h-screen">
      <Link
        className="rounded-md bg-black p-2 text-center text-white"
        href={"themeswitch"}
      >
        Screen Theme switch
      </Link>
      <Link
        className="rounded-md bg-black p-2 text-center text-white"
        href={"multilanguage"}
      >
        Screen multi language
      </Link>
      <Link
        className="rounded-md bg-black p-2 text-center text-white"
        href={"modalexample"}
      >
        Screen modal example
      </Link>
      <Link
        className="rounded-md bg-black p-2 text-center text-white"
        href={"drawerscreen"}
      >
        Screen drawer
      </Link>
      <Link
        className="rounded-md bg-black p-2 text-center text-white"
        href={"tabnavigatorscreen"}
      >
        Screen bottomnavigator
      </Link>
      <Link
        className="rounded-md bg-black p-2 text-center text-white"
        href={"alertdialogscreen"}
      >
        Screen alert dialog
      </Link>
      <Link
        className="rounded-md bg-black p-2 text-center text-white"
        href={"formscreen"}
      >
        Screen form
      </Link>
      <Link
        className="rounded-md bg-black p-2 text-center text-white"
        href={"toastscreen"}
      >
        Screen toast
      </Link>
      <Link
        className="rounded-md bg-black p-2 text-center text-white"
        href={"imagepickerscreen"}
      >
        Screen image picker
      </Link>
      <Link
        className="rounded-md bg-black p-2 text-center text-white"
        href={"appbarscreen"}
      >
        Screen appbar
      </Link>
      <Link
        className="rounded-md bg-black p-2 text-center text-white"
        href={"pageviewscreen"}
      >
        Screen PageView
      </Link>
      <Link
        className="rounded-md bg-black p-2 text-center text-white"
        href={"carrouselscreen"}
      >
        Screen Carrousel
      </Link>
    </View>
  );
};

export default indexScreen;
