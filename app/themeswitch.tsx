import { useColorScheme } from 'nativewind';
import { Switch, Text, View } from 'react-native';

const ThemeSwitchScreen = () => {
	const { colorScheme, toggleColorScheme } = useColorScheme();
	return (
		<View className="grid justify-center items-start bg-blue-700 dark:bg-blue-950 h-screen place">
			<Switch
			className='m-auto'
				value={colorScheme == 'dark'}
				onChange={toggleColorScheme}
			></Switch>
		</View>
	);
};

export default ThemeSwitchScreen;
